app_name = 'shelf'


CONFIG = {
    'BANNER': ('The National Cheese Emporium', 'name of the shop'),
    'OWNER': ('Mr. Henry Wensleydale', 'owner of the shop'),
    'OWNER_EMAIL': ('henry@example.com', 'contact email for owner', 'email'),
}
