from .conf import conf

__version__ = '0.2.0'

default_app_config = 'constance_register.apps.ConstanceRegisterConfig'
