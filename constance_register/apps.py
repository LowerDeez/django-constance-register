from django.apps import AppConfig as BaseAppConfig


class ConstanceRegisterConfig(BaseAppConfig):
    name = 'constance_register'
